from django.urls import path, include
from . import views
from .views import *

app_name = 'lab1'

urlpatterns = [
    path('', views.index, name='index'),
    path('profile', views.profile, name='profile'),
    path('secret', views.secret, name='secret'),
]